const express = require("express")
const config = require("config")
const mongoose = require("mongoose")


const PORT = config.get("port") || 5000

const app = express()

app.use(express.json({ extended: true }))

// Настройка роутов

app.use("/api/auth", require("./routes/auth.routes"))

async function start () {
    try{
      await mongoose.connect(config.get("mongoUri"), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      })


      app.listen(PORT, () => console.log(`App hase been started on port ${PORT}...`))
    } catch (e) {
      console.log("Server Error", e.message)
      process.exit(0)
    }
}

start()

